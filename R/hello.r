#' Hello, world!
#'
#' This is an example function named 'hello' which prints 'Hello, world!'.
#'
#' @param print \code{TRUE} or \code{FALSE}: flag to print 'Hello, world!'.
#'
#' @returns Returns a character string.
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # Hello world example
#' #----------------------------------------------------------------------------
#' library(emptypkg)
#' hello()
#'
#' @export
hello <- function(print = TRUE) {
  if (print) {
    paste("Hello, world!")
  }
}
