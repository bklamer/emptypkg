## Release summary

This is the first release to CRAN.

## Test environments

- local ubuntu 16.04, R 3.4.1
- local Windows 10, R 3.4.1

## R CMD check results

0 errors | 0 warnings | 0 notes

R CMD check succeeded

## Downstream dependencies

There are currently no downstream dependencies for this package
