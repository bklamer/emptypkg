# emptypkg

## Overview

`emptypkg` provides a template for creating R packages. It is based on
the book [R Packages](http://r-pkgs.had.co.nz/) and the workflow from
[devtools](https://github.com/hadley/devtools). Have fun :)

## Installation

Installation goes here…

## Usage

Usage examples go here…
